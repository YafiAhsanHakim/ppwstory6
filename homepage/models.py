from django.db import models
from django.forms import ModelForm

# Create your models here.
class Status(models.Model):
	status = models.CharField('status', max_length=100)
	
	def __str__(self):
		return self.status